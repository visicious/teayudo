import React, {Component} from 'react';
import {ActivityIndicator, AsyncStorage, Platform, StyleSheet, Text, View, Button} from 'react-native';
//import OAuthManager from 'react-native-oauth';
import {Router, Scene} from 'react-native-router-flux';
import Authentication from './routes/Authentication';
import MapNapay from './routes/mapNapay';
import {savedPointsPath, serverBaseUrl} from './constants/constants';

/*const manager = new OAuthManager('firestackexample')
manager.configure({
  facebook: {
    client_id: '578478032621140',
    client_secret: '2c6e4dde7503c49a9c91f5294eeeb839'
  },
  google: {
    callback_url: `127.0.0.1:8081/google`,
    client_id: '391580961815-o4q1s0vuhq5ujmdhj6bed6tqrgug5j4l.apps.googleusercontent.com',
    client_secret: '5oRUDMk0Q2XNv9-Vf4-kO41E'
  }
});*/

//TODO: Add a kind of action with these pieces of code to kind redirect when clinking
//      in a button to obtain the data 
/*manager.authorize('google', {scopes: 'email,profile'})
.then(resp => console.log('Your users ID'))
.catch(err => console.log('There was an error'));

manager.authorize('facebook', {scopes: 'email,profile'})
.then(resp => console.log('Your users ID'))
.catch(err => console.log('There was an error'));*/

type Props = {};
export default class App extends Component<Props> {
  constructor() {
    super();
    this.state = { isLoaded: false };
  }

  componentDidMount() {
    AsyncStorage.getItem('id_token').then((token) => {    
      this.setState({ isLoaded: true });

      if (token != null) {
        Actions.MapNapay();
      }
    }).catch((err) => {
      this.setState({ isLoaded: true });
    });
  }

  render() {
    if (!this.state.isLoaded) {
      return (
        <View style={{ justifyContent: 'center', flex: 1,position: 'absolute',left: 0,top: 0,opacity: 0.5,backgroundColor: 'white',width: '100%', height: '100%' }}>
          <ActivityIndicator size="large" color="#00ff00" />
        </View>
      )
    } else {
      return(
        <Router>
          <Scene key='root'>
            <Scene
              component={Authentication}
              hideNavBar={true}
              key='Authentication'
              title='Authentication'
            />
            <Scene
              component={MapNapay}
              initial={true}
              hideNavBar={true}
              key='MapNapay'
              title='Napay Travel'
            />
          </Scene>
        </Router>
      )
    }
  }
}