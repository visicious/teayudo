import React, {Component} from 'react';
import {Alert, AsyncStorage, Image, ImageBackground, Text, TextInput, TouchableOpacity, View} from 'react-native';
import {Actions} from 'react-native-router-flux';
import styles from '../styles/styles';
import {serverBaseUrl} from '../constants/constants';

type Props = {};
export default class Authentication extends Component<Props> {

  constructor() {
    super();
    this.state = { username: null, password: null, name:null, age:null, nationality:null };
  }

  userSignup() {
    if (!this.state.username || !this.state.password || !this.state.name || !this.state.age || !this.state.nationality) {
      Alert.alert('Error', 'Please fill every input before submit your sign up'); 
      return;
    }

    fetch(serverBaseUrl+'/api/v2/users/add_user', {
      method: 'POST',
      headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
      body: JSON.stringify({
        username: this.state.username,
        password: this.state.password,
        name: this.state.name,
        age: this.state.age,
        nationality: this.state.nationality,
        token:  'WrfsQG5ROF8kTuwFU1HBWVKngzNNBY7v+',
      })
    })
    .catch((err) => {
      Alert.alert('Network Error', 'You need Internet connection for Login');
      return;
    })
    .then((response) => response.json())
    .then((responseData) => {
      if(responseData.code == '403') {
        Alert.alert( 'Error', responseData.message);
      }
      else {
        this.saveItem('id_token', responseData.id_token);
        Alert.alert( 'Signup Success!', 'Welcome dear pathfinder!');
        Actions.MapNapay();
      }
    })
    .catch((err) => {
      console.error(err);
      Alert.alert('Error', 'Please fill every input before submit your Sign up');
    })
    .done();
  }

  userLogin() {
    if (!this.state.username || !this.state.password) {
      Alert.alert('Error', 'Please fill username and password inputs');
      return;
    }

    fetch(serverBaseUrl+'/api/v2/users/login', {
      method: 'POST',
      headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
      body: JSON.stringify({
        username: this.state.username,
        password: this.state.password,
        token:  'WrfsQG5ROF8kTuwFU1HBWVKngzNNBY7v+',
      })
    })
    .catch((err) => {
      Alert.alert('Network Error', 'You need Internet connection for Login');
      return;
    })
    .then((response) => response.json())
    .then((responseData) => {
      this.saveItem('id_token', responseData.id_token);
      Alert.alert('Login Success!', 'Welcome back dear pathfinder!');
      Actions.MapNapay();
    })
    .catch((err) => {
      Alert.alert('Error', 'Username or password are incorrect. Please check');
    })
    .done();
  }

  async saveItem(item, selectedValue) {
    try {
      await AsyncStorage.setItem(item, selectedValue);
    } catch (error) {
      console.error('AsyncStorage error: ' + error.message);
    }
  }

  render() {
    return (
      <ImageBackground
        source={require('../images/napaytravel-background.png')}
        imageStyle={{resizeMode: 'cover'}}
        style={styles.homeBackgroundImage}
      >
        <View style={styles.container}>
          <Image source={require('../images/napaytravel-logo.png')} style={styles.imageLogo}/>


          <View style={styles.form}>
            <TextInput
              editable={true}
              onChangeText={(name) => this.setState({name})}
              placeholder='Complete Name'
              ref='name'
              returnKeyType='next'
              style={styles.inputText}
              value={this.state.name}
            />

            <TextInput
              editable={true}
              onChangeText={(age) => this.setState({age})}
              placeholder='Age'
              ref='age'
              keyboardType = 'numeric'
              returnKeyType='next'
              style={styles.inputText}
              value={this.state.age}
            />

            <TextInput
              editable={true}
              onChangeText={(nationality) => this.setState({nationality})}
              placeholder='Nationality'
              ref='nationality'
              returnKeyType='next'
              style={styles.inputText}
              value={this.state.nationality}
            />

            <TextInput
              editable={true}
              onChangeText={(username) => this.setState({username})}
              placeholder='Username'
              ref='username'
              returnKeyType='next'
              style={styles.inputText}
              value={this.state.username}
            />

            <TextInput
              editable={true}
              onChangeText={(password) => this.setState({password})}
              placeholder='Password'
              ref='password'
              returnKeyType='next'
              secureTextEntry={true}
              style={styles.inputText}
              value={this.state.password}
            />
          

            <TouchableOpacity style={styles.secondaryButtonWrapper} onPress={this.userLogin.bind(this)}>
              <Text style={styles.buttonText}> Log In </Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.buttonWrapper} onPress={this.userSignup.bind(this)}>
              <Text style={styles.buttonText}> Sign Up </Text>
            </TouchableOpacity>
          </View>
        </View>
      </ImageBackground>
    );
  }
}
